document.addEventListener('DOMContentLoaded', function() {
  (function(){

    let menuBtn = document.querySelectorAll('.header-droplink');

    // for (let i = 0; i < menuBtn.length; i++) {
    //   menuBtn[i].classList.remove('active')
    //   let self = menuBtn[i];
    //   self.addEventListener('mouseover', (e) => {
    //     e.stopPropagation;
    //     self.classList.add('active');
    //   });
    // }

    // menuBtn.forEach( (btn, index) => {
    //   let menuItem = index;
    //   btn.addEventListener('mouseover', (e) => {
    //     btn.classList.remove('active');
    //     btn[menuItem].classList.add('active');
    //     e.stopPropagation();
    //   });
    // });

    // close on document click
    document.addEventListener('click', function(){
      menuBtn.forEach( (btn) => {
        btn.classList.remove('active')
      });
    }, false);

    // close on scroll
    document.addEventListener('scroll', function(){
      menuBtn.forEach( (btn) => {
        btn.classList.remove('active')
      });
    }, false);

  })();
});
