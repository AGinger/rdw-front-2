document.addEventListener('DOMContentLoaded', function() {

  (function () {

    let mainImg = document.getElementById('mainImg'),
        galleryItem = document.querySelectorAll('.gallery-ctrl-img'),
        galleryImg = document.querySelectorAll('.gallery-ctrl-img img');

    galleryImg.forEach((item) => {
      item.addEventListener('mouseover', () => {
        mainImg.src = item.src;
      });
    });

    galleryItem.forEach((item) => {
      item.addEventListener('mouseover', () => {
        for( i = 0; i < galleryItem.length; i++ ) {
          galleryItem[i].classList.remove('active');
        }
        item.classList.add('active')
      });
    });

  })();

});