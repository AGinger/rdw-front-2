document.addEventListener('DOMContentLoaded', function() {

  (function () {

    let searchBtn = document.querySelector('.auth-search-icon'),
        searchBlock = document.querySelector('.auth-search-field');

    searchBtn.addEventListener('click', (e) => {
      searchBlock.classList.toggle('active')
      e.stopPropagation();
    })

    document.addEventListener("click", function(){
        searchBlock.classList.remove('active')
    }, false);

    searchBlock.addEventListener("click", function(e){
      // Show the menus
      searchBlock.classList.add('active')
      e.stopPropagation();
    }, false);

  })();

});